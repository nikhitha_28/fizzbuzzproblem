package com.thoughtworks.fizzbuzz;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FizzBuzzTest {
    @Test
    void shouldPrintFizzWhenTheGivenInputIsDivisibleByThree(){
        FizzBuzz fizzbuzz=new FizzBuzz();

        String expectedResult= FizzBuzz.divisibleByThree(3);
        String actualResult="fizz";

        assertThat(expectedResult,is(equalTo(actualResult)));
    }
    @Test
    void shouldPrintFizzWhenTheGivenInputIsDivisibleByFive(){
        FizzBuzz fizzbuzz=new FizzBuzz();

        String expectedResult= FizzBuzz.divisibleByFive(5);
        String actualResult="Buzz";

        assertThat(expectedResult,is(equalTo(actualResult)));
    }
    @Test
    void shouldPrintFizzWhenTheGivenInputIsDivisibleByBothThreeAndFive(){
        FizzBuzz fizzbuzz=new FizzBuzz();

        String expectedResult=FizzBuzz.divisibleByThreeAndFive(15);
        String actualResult="fizzBuzz";

        assertThat(expectedResult,is(equalTo(actualResult)));
    }
    @Test
    void shouldPrintFizzWhenTheGivenInputIsNotDivisibleByEitherBothThreeOrFive(){
        FizzBuzz fizzbuzz=new FizzBuzz();

        int expectedResult=FizzBuzz.notDivisibleByThreeOrFive(17);
        int actualResult=17;

        assertThat(expectedResult,is(equalTo(actualResult)));
    }
}
